package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;

import javax.swing.plaf.synth.SynthSpinnerUI;
import javax.swing.text.html.HTMLDocument.Iterator;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;
import com.sun.xml.internal.bind.v2.model.core.ID;

import api.IDivvyTripsManager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import sun.awt.image.ImageWatched.Link;
import sun.misc.FloatingDecimal.BinaryToASCIIConverter;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.Stack;

public class DivvyTripsManager<T> implements IDivvyTripsManager {

	private LinkedList<Station> stations;
	private LinkedList<Trip> trips;
	private LinkedList<Bike> bikes;
	private RedBlackBST<Integer, String> arbol;
	private int cont =0;


	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = "./data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = "./data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3		
	// TODO Actualizar	
	public static final String TRIPS_Q3 = "./data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = "./data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String CLICLORUTAS = "./data/CDOT_Bike_Routes_2014_1216-transformed.json";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = "./data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = "./data/Divvy_Stations_2017_Q3Q4.csv";


	public DivvyTripsManager()
	{
		arbol = new RedBlackBST<>();
		stations = new LinkedList<>();
		trips= new LinkedList<>();
		bikes= new LinkedList<Bike>();
	}

	public void loadTripsQ1() {
		// TODO Auto-generated method stub
		int contador=0;
		int contador2=0;
		try {
			FileReader h = new FileReader(TRIPS_Q1);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.split(",");
				String pTripIdBF = texto[0].replace("\"","");
				String pStartTimeBF = texto[1].replace("\"","");
				String pFinalTimeBF = texto[2].replace("\"","");
				String pBikeIdBF = texto[3].replace("\"","");
				String pTripDurationBF = texto[4].replace("\"","");
				String pFromStationIdBF = texto[5].replace("\"","");
				String pToStationIdBF = texto[7].replace("\"","");
				String pUserType = texto[9].replace("\"","");
				String pGender = "";
				if(pUserType.equals("Subscriber"))
				{
					pGender = texto[10];
				}

				int pTripId= Integer.parseInt(pTripIdBF);
				Date fec = null;
				Date fec2 = null;
				try {
					fec = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pStartTimeBF);
					fec2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pFinalTimeBF);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				int pBikeId= Integer.parseInt(pBikeIdBF);
				int pTripDuration=Integer.parseInt(pTripDurationBF);
				int pFromStationId= Integer.parseInt(pFromStationIdBF);
				int pToStationId= Integer.parseInt(pToStationIdBF);

				Trip a = new Trip(pTripId, fec, fec2, pBikeId, pTripDuration, pFromStationId, pToStationId, pGender);
				if(contador == 0)
				{
					trips.addFirst(a);
					contador++;
				}
				else{
					trips.append(a);
					contador2++;
				}

				entrada = bf.readLine();
			}

			bf.close();
			h.close();
			System.out.println("se cargaron " + trips.size() + " datos trips");
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}


	}

	@Override
	public void loadTripsQ2() {
		// TODO Auto-generated method stub
		int contador=0;
		int contador2=1;
		try {
			FileReader h = new FileReader(TRIPS_Q2);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.split(",");
				String pTripIdBF = texto[0].replace("\"","");
				String pStartTimeBF = texto[1].replace("\"","");
				String pFinalTimeBF = texto[2].replace("\"","");
				String pBikeIdBF = texto[3].replace("\"","");
				String pTripDurationBF = texto[4].replace("\"","");
				String pFromStationIdBF = texto[5].replace("\"","");
				String pToStationIdBF = texto[7].replace("\"","");
				String pUserType = texto[9].replace("\"","");
				String pGender = "";
				if(pUserType.equals("Subscriber"))
				{
					pGender = texto[10];
				}

				int pTripId= Integer.parseInt(pTripIdBF);
				int pBikeId= Integer.parseInt(pBikeIdBF);
				int pTripDuration=Integer.parseInt(pTripDurationBF);
				int pFromStationId= Integer.parseInt(pFromStationIdBF);
				int pToStationId= Integer.parseInt(pToStationIdBF);
				Date fec = null;
				Date fec2 = null;
				try {
					fec = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pStartTimeBF);
					fec2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pFinalTimeBF);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Trip a = new Trip(pTripId, fec, fec2, pBikeId, pTripDuration, pFromStationId, pToStationId, pGender);
				if(contador == 0)
				{
					trips.addFirst(a);
					contador++;
				}
				else{
					trips.append(a);
					contador2++;
				}
				entrada = bf.readLine();
			}

			bf.close();
			h.close();
			System.out.println("se cargaron " + trips.size() + " datos trips");
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}


	}

	@Override
	public void loadTripsQ3() {
		// TODO Auto-generated method stub
		int contador=0;
		int contador2=1;
		try {
			FileReader h = new FileReader(TRIPS_Q3);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.split(",");
				String pTripIdBF = texto[0].replace("\"","");
				String pStartTimeBF = texto[1].replace("\"","");
				String pFinalTimeBF = texto[2].replace("\"","");
				String pBikeIdBF = texto[3].replace("\"","");
				String pTripDurationBF = texto[4].replace("\"","");
				String pFromStationIdBF = texto[5].replace("\"","");
				String pToStationIdBF = texto[7].replace("\"","");
				String pUserType = texto[9].replace("\"","");
				String pGender = "";
				if(pUserType.equals("Subscriber"))
				{
					pGender = texto[10];
				}

				int pTripId= Integer.parseInt(pTripIdBF);
				int pBikeId= Integer.parseInt(pBikeIdBF);
				int pTripDuration=Integer.parseInt(pTripDurationBF);
				int pFromStationId= Integer.parseInt(pFromStationIdBF);
				int pToStationId= Integer.parseInt(pToStationIdBF);
				Date fec = null;
				Date fec2 = null;
				try {
					fec = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pStartTimeBF);
					fec2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pFinalTimeBF);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Trip a = new Trip(pTripId, fec, fec2, pBikeId, pTripDuration, pFromStationId, pToStationId, pGender);
				if(contador == 0)
				{
					trips.addFirst(a);
					contador++;
				}
				else{
					trips.append(a);
					contador2++;
				}

				entrada = bf.readLine();
			}

			bf.close();
			h.close();
			System.out.println("se cargaron " + trips.size() + " datos trips");
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}


	}

	@Override
	public void loadTripsQ4() {
		// TODO Auto-generated method stub
		int contador=0;
		int contador2=1;
		try {
			FileReader h = new FileReader(TRIPS_Q4);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.split(",");
				String pTripIdBF = texto[0].replace("\"","");
				String pStartTimeBF = texto[1].replace("\"","");
				String pFinalTimeBF = texto[2].replace("\"","");
				String pBikeIdBF = texto[3].replace("\"","");
				String pTripDurationBF = texto[4].replace("\"","");
				String pFromStationIdBF = texto[5].replace("\"","");
				String pToStationIdBF = texto[7].replace("\"","");
				String pUserType = texto[9].replace("\"","");
				String pGender = "";
				if(pUserType.equals("Subscriber"))
				{
					pGender = texto[10];
				}

				int pTripId= Integer.parseInt(pTripIdBF);

				int pBikeId= Integer.parseInt(pBikeIdBF);
				int pTripDuration=Integer.parseInt(pTripDurationBF);
				int pFromStationId= Integer.parseInt(pFromStationIdBF);
				int pToStationId= Integer.parseInt(pToStationIdBF);
				Date fec = null;
				Date fec2 = null;
				try {
					fec = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pStartTimeBF);
					fec2 = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(pFinalTimeBF);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Trip a = new Trip(pTripId, fec, fec2, pBikeId, pTripDuration, pFromStationId, pToStationId, pGender);
				if(contador == 0)
				{
					trips.addFirst(a);
					contador++;
				}
				else{
					trips.append(a);
					contador2++;
				}

				entrada = bf.readLine();
			}
			bf.close();
			h.close();
			System.out.println("se cargaron " + trips.size() + " datos trips");
		}
		catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public void loadStationsQ1Q2() {
		// TODO Auto-generated method stub
		int contador=0;
		int contador2=0;
		try {
			FileReader h = new FileReader(STATIONS_Q1_Q2);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.toString().split(",");
				String idBF = texto[0];
				String latBF = (texto[3]);
				String longiBF = (texto[4]);
				String capBF = (texto[5]);
				String dat = texto[6].replace("\"","");

				idBF=idBF.replace("\"","");
				latBF=latBF.replace("\"","");
				longiBF=longiBF.replace("\"","");
				capBF=capBF.replace("\"","");

				int id = Integer.parseInt(idBF);
				double lat = Double.parseDouble(latBF);
				double longi = Double.parseDouble(longiBF);
				int cap = Integer.parseInt(capBF);
				Date fec = null;
				try {
					fec = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(dat);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Station a = new Station(id, texto[1], texto[2], lat, longi, cap, fec);
				entrada = bf.readLine();

				if(contador == 0)
				{
					stations.addFirst(a);
					contador++;
				}
				else
					stations.append(a);

			}		
			bf.close();
			h.close();
			System.out.println("se cargaron " + stations.size() + " datos stations");
		}
		catch (IOException fnE) {
			System.out.println(fnE.getMessage());

		}

	}
	@Override
	public void loadStationsQ3Q4() {
		int contador=0;
		int contador2=0;
		try {
			FileReader h = new FileReader(STATIONS_Q3_Q4);
			BufferedReader bf = new BufferedReader(h);
			String entrada = bf.readLine();
			entrada = bf.readLine();
			while (entrada != null) {
				String[] texto = entrada.toString().split(",");
				int id = Integer.parseInt(texto[0]);
				double lat = Double.parseDouble(texto[3]);
				double longi = Double.parseDouble(texto[4]);
				int cap = Integer.parseInt(texto[5]);
				String dat = texto[6].replace("\"","");
				Date fec = null;
				try {
					fec = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(dat);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Station a = new Station(id, texto[1], texto[2], lat, longi, cap, fec);
				entrada = bf.readLine();

				if(contador == 0)
				{
					stations.addFirst(a);
					contador++;
				}
				else
					stations.append(a);

				contador2++;
			}		
			bf.close();
			h.close();
			System.out.println("se cargaron " + stations.size() + " datos station");
		}
		catch (IOException fnE) {
			System.out.println(fnE.getMessage());

		}

	}
	public double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(latitudReferencia-lat1); 

		double lonDistance = Math.toRadians(longitudReferencia-lon1); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}

	public Station darEstacionID(int pIdentificador)
	{
		for (Station actual : stations)
			if(actual.darId() == pIdentificador)
				return actual;

		return null;
	}

	//	public IDoublyLinkedList<Bike> darListaBikes()
	//	{
	//		LinkedList<Trip> lista= trips;
	//		for (int i = 0; i < lista.size(); i++) 
	//		{
	//			Trip actual = lista.get(i).getItem();
	//			int idActual=actual.getBikeId();
	//			int total=1;
	//			int duracionTotal=actual.getTripDuration();
	//			double distanciaTotal=0;
	//			for(int j=1; j< lista.size(); j++)
	//			{
	//				Trip actual2= lista.get(j).getItem();
	//				if(idActual==actual2.getBikeId())
	//				{
	//					Station id1 = darEstacionID(trips.get(i).getItem().getStartStationId());
	//					Station id2 = darEstacionID(trips.get(i).getItem().getEndStationId());
	//					distanciaTotal = calculateDistance(id1.latidude, id1.longitude, id2.latidude, id2.longitude);
	//					total++;
	//					duracionTotal+=actual2.getTripDuration();
	//				}
	//			}
	//			Bike biky= new Bike(idActual, total, distanciaTotal, duracionTotal);
	//			bikes.append(biky);
	//		}
	//		return bikes;

	//	}

	public RedBlackBST<Integer, String> crearArbol()
	{
		//		System.out.println("entro");
		//		IDoublyLinkedList<Bike> lista = darListaBikes();
		//		for(Bike actual: lista)
		//		{
		//			int id= actual.getBikeId();
		//			String valor= "total de viajes: "+actual.getTotalTrips()+", la distancia total es: "+actual.getTotalDistance()+", tiempo total es: "+actual.getTotalDuration();
		//			System.out.println(valor);
		//			arbol.put(id, valor);
		//		}
		//		return arbol;


		RedBlackBST<Integer, String> arbol = new RedBlackBST<>();
		for (Trip actual : trips) {
			if(arbol.contains(actual.getBikeId()))
			{
				//				System.out.println(actual.getBikeId());
				String a = arbol.get(actual.getBikeId());
				String[] arr = a.split(",");
				int numRecorridos = Integer.parseInt(arr[0]);
				int nuevoNum= numRecorridos + 1;
				double distRecorrida = Double.parseDouble(arr[1]);
				double nuevoDist = distRecorrida + actual.getTripDuration();
				long tiempoRecorr = Long.parseLong(arr[2]);
				long nuevoTiempoRecorr = tiempoRecorr + (Math.abs(actual.getStopTime().getTime() - actual.getStartTime().getTime()));
				//				arbol.delete(actual.getBikeId());
				if(actual.getBikeId() == 5420)
					System.out.println(arbol.get(actual.getBikeId()));
				arbol.put(actual.getBikeId(), "" + nuevoNum + "," + nuevoDist + "," + nuevoTiempoRecorr);
			}
			else
			{
				Station sta1 = darEstacionID(actual.getStartStationId());
				if(sta1 !=null)
				{
					double lat1 = sta1.latidude;
					double lon1 = sta1.longitude;
					Station sta2 = darEstacionID(actual.getEndStationId());
					if(sta2 != null)
					{
						double longitudReferencia = sta2.longitude;
						double latitudReferencia = sta2.latidude;
						arbol.put(actual.getBikeId(), "" + 1 + "," + (calculateDistance(lat1, lon1, longitudReferencia, latitudReferencia)) + "," + (Math.abs(actual.getStopTime().getTime() - actual.getStartTime().getTime())));
					}
				}
			}
		}

		System.out.println("size: "+arbol.size());
		System.out.println("altura: "+arbol.height());

		return arbol;
	}


	public String darBikePorId(int pId)
	{
		RedBlackBST<Integer, String> arbol = crearArbol();
		if(!arbol.contains(pId))
			return null;

		String a = arbol.get(pId);
		String[] b = a.split(",");
		String resp = ("El numero de viajes es " + b[0] + " la distancia es " + b[1] + "y el tiempo recorrido es " + b[2]);
		return resp;
	}

	public LinkedList<Integer> bicicletasEnRango(int pMenor, int pMayor)
	{
		RedBlackBST<Integer, String> arbol = crearArbol();
		Iterable<Integer> it= arbol.keys(pMenor, pMayor);
		LinkedList<Integer> lista= new LinkedList<>();

		for(int actual: it)
		{
			lista.addFirst(actual);
		}

		return lista;
	}
}