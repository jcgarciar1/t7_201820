package model.vo;

import java.util.Date;

public class Station{
	public int id;
	
	public String name;
	
	public String city;
	
	public double latidude;
	
	public double longitude;
	
	public int capacity;
	
	public Date date;
	
	public Station(int num,String pName,String ciudad,double lat, double longi, int cap,Date fecha)
	{
		id = num;
		name = pName;
		city = ciudad;
		latidude = lat;
		longitude = longi;
		capacity = cap;
		date = fecha;
	}
	
	public int darId()
	{
		return id;
	}
	
	public String darNombre()
	{
		return name;
	}
	public String darCiudad()
	{
		return city;
	}
	
	public double darLatitud()
	{
		return latidude;
	}
	
	public double darLongitud()
	{
		return longitude;
	}
	
	public int darCapacidad()
	{
		return capacity;
	}
	
	public Date darFecha()
	{
		return date;
	}
	public double calculateDistance (Station o2) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(o2.latidude-this.latidude); 

		double lonDistance = Math.toRadians(o2.longitude-this.longitude); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(this.latidude))* Math.cos(Math.toRadians(o2.latidude)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}

}
