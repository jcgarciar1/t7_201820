package model.vo;

import java.util.Iterator;

public class Bike implements Comparable<Bike>{

	private int bikeId;
    private int totalTrips;
    private double totalDistance;
    private int totalDuration;

    public Bike(int bikeId, int totalTrips, double totalDistance, int ptotalDuration) {
        this.bikeId = bikeId;
        this.totalTrips = totalTrips;
        this.totalDistance = totalDistance;
        this.totalDuration = ptotalDuration;
    }

    @Override
    public int compareTo(Bike o) {
    	if(this.totalTrips < o.getTotalTrips() )
    		return -1;
    	else if(this.totalTrips > o.getTotalTrips())
    		return 1;
    	else
    		return 0;
    }
    
    public int getBikeId() {
        return bikeId;
    }

    public int getTotalTrips() {
        return totalTrips;
    }

    public double getTotalDistance() {
        return totalDistance;
    }
    public int getTotalDuration() {
    	return totalDuration;
    }
}
