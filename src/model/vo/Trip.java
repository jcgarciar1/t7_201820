package model.vo;

import java.time.LocalDateTime;
import java.util.Date;

public class Trip implements Comparable<Trip> {

	public final static String MALE = "male";
	public final static String FEMALE = "female";
	public final static String UNKNOWN = "unknown";

	private int tripId;
	private Date startTime;
	private Date stopTime;

	private int bikeId;
	private int tripDuration;
	private int startStationId;
	private int endStationId;
	private String gender;

	public Trip(int tripId, Date startTime, Date stopTime, int bikeId, int tripDuration, int startStationId, int endStationId, String gender) {
		this.tripId = tripId;
		this.startTime = startTime;
		this.stopTime = stopTime;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.startStationId = startStationId;
		this.endStationId = endStationId;
		this.gender = gender;
	}

	@Override
	public int compareTo(Trip o) {
		// TODO completar
		if(this.startTime.compareTo(o.getStartTime()) <0)
			return -1;
		else if(this.startTime.compareTo(o.getStartTime())>0)
			return 1;
		else
			return 0;
	}

	public int getTripId() {
		return tripId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public Date getStopTime() {
		return stopTime;
	}

	public int getBikeId() {
		return bikeId;
	}

	public int getTripDuration() {
		return tripDuration;
	}

	public int getStartStationId() {
		return startStationId;
	}

	public int getEndStationId() {
		return endStationId;
	}

	public String getGender() {
		return gender;
	}
}
