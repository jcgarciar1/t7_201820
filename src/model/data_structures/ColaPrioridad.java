package model.data_structures;

public class ColaPrioridad<T extends Comparable<T>>  implements IColaPrioridad<T>{

	private T[] pq;          // elements
	private int n;             // number of elements
	private int tamMax;

	// set inititial size of heap to hold size elements
	public ColaPrioridad(int capacity) {
		tamMax = capacity;
		pq = (T[]) (new Comparable[tamMax]);
		n = 0;
	}
	public ColaPrioridad(Object[] array) {
		tamMax = array.length + 1;
		pq = (T[]) (new Comparable[tamMax]);
		n = array.length;
		for (int i = 0; i < n; i++)
		{
			agregar((T)array[i]);
		}

	}


	public boolean esVacia() { return n == 0;  }
	public int darNumeroElementos()        { return n;       } 
	public T delMax()      { return pq[--n]; }
	public T max()      { return pq[n]; }

	public void agregar(T key) {
		int i = n-1;
		while (i >= 0 && less(key, pq[i])) {
			pq[i+1] = pq[i];
			i--;
		}
		pq[i+1] = key;
		n++;
	}



	/***************************************************************************
	 * Helper functions.
	 ***************************************************************************/
	private boolean less(T v, T w) {
		return v.compareTo(w) < 0;
	}


	@Override
	public int tamanoMax() {
		// TODO Auto-generated method stub
		return tamMax;
	}

}
