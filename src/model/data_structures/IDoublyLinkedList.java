package model.data_structures;

import java.util.Iterator;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	//TODO Agregar API de DoublyLinkedList
Integer getSize();
	
	public void addFirst(T item);
	public void append(T item);
	public void addAtPosition(T item, int i);
	public void removeFirst();
	public void remove (int pos);
	public Node<T> get (int pos);
	public int size();
	public Node<T> next();
	public Node<T> previous();
	public boolean isEmpty();
	public Node<T> darPrimero();
	public Node<T> darUltimo();

	public Iterator<T> iterator();
}
