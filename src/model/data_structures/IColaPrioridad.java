package model.data_structures;

public interface IColaPrioridad<T> {
	public int tamanoMax();
	public void agregar(T elemento);
	public boolean esVacia();
	public int darNumeroElementos();
	public T max();

}
