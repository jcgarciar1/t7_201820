package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements IDoublyLinkedList<T>, Iterable<T>
{
	private Node<T> list; 
	private int listSize;
	private Node<T> ultimo;

	//Tomado de presentaciones en clase

	public LinkedList()
	{
		list = null;
		ultimo = null;
	}

	public LinkedList(T item)
	{
		list = new Node<T> (item);
		ultimo = list;
		listSize = 1;
	}
	@Override
	public Integer getSize() {
		return listSize;
	}

	@Override
	public void addFirst(T item) {

		Node <T> newHead = new Node<T> (item); 
		if(list == null){
			list = newHead;
			ultimo = list;
		}
		else
		{
			newHead.setNextNode(list);
			list.setPreviousNode(newHead);
			list = newHead;
		}
		listSize++;
	}

	@Override
	public void append(T item) {
		Node <T> newNode = new Node<> (item); 
		if(list == null)
		{
			list = newNode;
			ultimo = list;
		}
		else {
			ultimo.setNextNode(newNode);
			newNode.setPreviousNode(ultimo);
			ultimo = newNode;
		}
		listSize++;
	}

	@Override
	public void removeFirst() {
		list = list.getNext();
		if(list != null)
			list.setPreviousNode(null);
		listSize--;
	}

	@Override
	public void remove(int pos) {
		if(listSize > pos)
		{
			Node prev = get(pos).getPrevious();
			Node nex = get(pos).getNext();
			if(prev == null){
				removeFirst();
				return;
			}
			if(nex != null)
			{
				nex.setPreviousNode(prev);
			}
			if(prev != null)
			{
				prev.setNextNode(nex);
				if(prev.getNext() == null)
					ultimo = prev;
			}
		}

	}

	@Override
	public Node<T> get(int pos) {
		int contador = 0;
		Node <T> actual = list; 
		while(actual.getNext() != null && contador<pos && pos<=listSize) {
			actual = actual.getNext();
			contador++;
		}
		return actual;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return listSize;
	}

	@Override
	public boolean isEmpty() {
		boolean a = false;
		if(list == null)
			a = true;

		return a;
	}

	@Override
	public void addAtPosition(T item, int i) {
		Node<T> nuevo = new Node<T>(item);
		Node<T> actual = get(i);
		if(actual == null)
		{
			append(item);
			return;
		}
		if(actual.getPrevious() != null)
		{
			nuevo.setPreviousNode(actual.getPrevious());
			actual.getPrevious().setNextNode(nuevo);
		}
		else
			list = nuevo;

		actual.setPreviousNode(nuevo);
		nuevo.setNextNode(actual);
		listSize++;
	}


	@Override
	public Node next() {
		return list.getNext();
	}

	@Override
	public Node previous() {
		return list.getPrevious();
	}

	public Node darPrimero()
	{
		return list;
	}

	public Node darUltimo()
	{
		return ultimo;
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() 
		{
			Node<T> act= list;
			public boolean hasNext()
			{
				if (listSize == 0)
				return false;
				if (act == null)
					return true;
				return act.getNext() != null;
			}
			public T next()
			{
				if (act == null)
					act = list;
				else
					act = act.getNext();
				return act.getItem();

			}
		};
	}
	}