package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements IQueue<T>, Iterable<T>{

	private Node<T> first;
	private Node<T> last;
	private int size = 0;
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() 
		{
			Node<T> act= first;
			public boolean hasNext()
			{
				if (size == 0)
				return false;
				if (act == null)
					return true;
				return act.getNext() != null;
			}
			public T next()
			{
				if (act == null)
					act = first;
				else
					act = act.getNext();
				return act.getItem();

			}
		};
	}
	

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub

		return (first == null);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void enqueue(T elem) {
		// TODO Auto-generated method stub
		Node <T> newNode = new Node<> (elem);
		if(size == 0)
		{
			first = newNode;
			last = newNode;
		}
		else
		{
			Node<T> oldLast = last;
			oldLast.setNextNode(newNode);
			last = newNode;
		}
		size++;
	}

	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		if (size == 0)
			return null;

		Node <T> oldFirst = first; 
		T elem = first.getItem(); 
		first = oldFirst.getNext(); 
		oldFirst.setNextNode(null);
		size--;
		return elem;	
	}
	public Node<T> darPrimero()
	{
		return first;
	}
	
	public Node<T> darUltimo()
	{
		return last;
	}

}
