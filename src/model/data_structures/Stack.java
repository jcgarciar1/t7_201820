package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Stack<T> implements IStack<T>{

	private Node<T> topStack;
	private int size = 0;
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>() 
		{
			Node<T> act= topStack;
			public boolean hasNext()
			{
				if (size == 0)
				return false;
				if (act == null)
					return true;
				return act.getNext() != null;
			}
			public T next()
			{
				if (act == null)
					act = topStack;
				else
					act = act.getNext();
				return act.getItem();

			}
		};
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return (topStack == null);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void push(T elem) {
		// TODO Auto-generated method stub
		Node <T> newNode = new Node<> (elem); 
		if (topStack == null )
		{
			topStack = newNode; 
		}
		else 
		{
			newNode.setNextNode(topStack); 
			topStack = newNode;
		}
		size++;
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		if (topStack == null )
			throw new EmptyStackException();
			T elem = topStack.getItem();
			
			Node <T> nexTop = topStack.getNext(); 
			topStack.setNextNode(null);
			topStack = nexTop;
			size--;
			return elem;
	}
	
	public Node<T> darPrimero()
	{
		return topStack;
	}
}
