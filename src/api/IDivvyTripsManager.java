package api;

import java.util.Date;

import model.data_structures.IDoublyLinkedList;
import model.data_structures.LinkedList;
import model.data_structures.RedBlackBST;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	void loadTripsQ1();
	
	void loadTripsQ2();
	
	void loadTripsQ3();
	
	void loadTripsQ4();
	
	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */
	void loadStationsQ1Q2();
	
	void loadStationsQ3Q4();
	
	RedBlackBST<Integer, String> crearArbol();
	
	String darBikePorId(int pId);
	
	LinkedList<Integer> bicicletasEnRango(int pMenor, int pMayor);

}
