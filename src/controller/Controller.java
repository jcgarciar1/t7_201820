package controller;

import java.time.LocalDateTime;
import java.util.Date;

import api.IDivvyTripsManager;
import model.data_structures.IDoublyLinkedList;

import model.data_structures.IQueue;
import model.data_structures.LinkedList;
import model.data_structures.RedBlackBST;
import model.logic.DivvyTripsManager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;


public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();
	
	public static void loadStationsQ1Q2() 
	{
		manager.loadStationsQ1Q2( );
	}
	public static void loadStationsQ3Q4() 
	{
		manager.loadStationsQ3Q4();
	}
	
	public static void loadTripsQ1() 
	{
		manager.loadTripsQ1();
	}
	
	public static void loadTripsQ2() 
	{
		manager.loadTripsQ2();
	}
	
	public static void loadTripsQ3() 
	{
		manager.loadTripsQ3();
	}
	
	public static void loadTripsQ4() 
	{
		manager.loadTripsQ4();
	}
	
	
	public static RedBlackBST<Integer, String> crearArbol()
	{
		return manager.crearArbol();
	}
	
	public static String darBikePorId(int pId)
	{
		return manager.darBikePorId(pId);
	}
	
	public static LinkedList<Integer> bicicletasEnRango(int pMenor, int pMayor)
	{
		return manager.bicicletasEnRango(pMenor, pMayor);
	}

	
}
