package view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IQueue;
import model.data_structures.LinkedList;
import model.data_structures.RedBlackBST;
import model.logic.DivvyTripsManager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		Controller controlador = new Controller();
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();

			int option = linea.nextInt();
			switch(option)
			{
			
			case 1:  //Carga de datos 1C
				String dataTrips = "";  // ruta del archivo de Trips
				String dataStations = ""; // ruta del archivo de Stations
				boolean reiniciarDatos = false;
				printMenuCargar();
				int tamanoDatos = linea.nextInt();
				switch (tamanoDatos)
				{
				case 1:
					dataTrips = DivvyTripsManager.TRIPS_Q1;
					dataStations = DivvyTripsManager.STATIONS_Q1_Q2;
					Controller.loadStationsQ1Q2();
					Controller.loadTripsQ1();
					break;
				case 2:
					dataTrips = DivvyTripsManager.TRIPS_Q2;
					dataStations = DivvyTripsManager.STATIONS_Q1_Q2;
					Controller.loadStationsQ1Q2();
					Controller.loadTripsQ2();
					break;
				case 3:
					dataTrips = DivvyTripsManager.TRIPS_Q3;
					dataStations = DivvyTripsManager.STATIONS_Q3_Q4;
					Controller.loadStationsQ3Q4();
					Controller.loadTripsQ3();
					break;
				case 4:
					dataTrips = DivvyTripsManager.TRIPS_Q4;
					dataStations = DivvyTripsManager.STATIONS_Q3_Q4;
					Controller.loadStationsQ3Q4();
					Controller.loadTripsQ4();
					break;
				case 5: // Opcion para reiniciar los datos del sistema. Conjunto vacio de trips y de estaciones.
					dataTrips = "";
					dataStations = "";
					reiniciarDatos = true;
					break;
				}

				if (!reiniciarDatos)
				{
					System.out.println("Trips x cargar al sistema: " + dataTrips);
					System.out.println("Stations x cargar al sistema: " + dataStations);
				}
				
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();
				
				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
				break;
				
				
			case 2: //Req 1
				
				Controller.crearArbol();
				
				break;
			
			case 3: //Req 2
				//Fecha Inicial
				System.out.println("Ingrese el id de la bicicleta (Ej: 125)");
				int idBicicleta = Integer.parseInt(linea.next());
				
				System.out.println(Controller.darBikePorId(idBicicleta));
				
				break;
				
			case 4: //Req 3A
				//Id de la bicicleta
				System.out.println("Ingrese el id menor: ");
				int idMinBicicleta = Integer.parseInt(linea.next());
				
				System.out.println("Ingrese el id mayor: ");
				int idMaxBicicleta = Integer.parseInt(linea.next());
				
				LinkedList<Integer> listaDeId= Controller.bicicletasEnRango(idMinBicicleta, idMaxBicicleta);
				for(int actual: listaDeId)
				{
					System.out.println("Lista de ids: " + actual);
				}
				
				break;
				
			case 5: //Salir
				fin = true;
				linea.close();
				break;
			
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de Datos----------");
		System.out.println("-------------------- Proyecto 1   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Actualizar la informacion del sistema con una fuente de datos (2017-Q1, 2017-Q2, 2017-Q3, 2017-Q4)");

		System.out.println("2. Obtener el arbol con las bicicletas");
		System.out.println("3. Obtener los datos de una bicicleta con un id dado");
		System.out.println("4. Obtener los id de las bicicletas dentro de un rango de id dado");
		
		System.out.println("5. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea agregar a los datos del sistema (carga incremental)?");
		System.out.println("-- 1. 2017-Q1");
		System.out.println("-- 2. 2017-Q2");
		System.out.println("-- 3. 2017-Q3");
		System.out.println("-- 4. 2017-Q4");
		System.out.println("-- 5. Reiniciar datos del sistema");		
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}
	
	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static Date convertirFecha_Hora_LDT(String fecha, String hora)
	{
//		String[] datosFecha = fecha.split("/");
//		String[] datosHora = hora.split(":");
//
//		int agno = Integer.parseInt(datosFecha[2]);
//		int mes = Integer.parseInt(datosFecha[1]);
//		int dia = Integer.parseInt(datosFecha[0]);
//		int horas = Integer.parseInt(datosHora[0]);
//		int minutos = Integer.parseInt(datosHora[1]);
//		int segundos = Integer.parseInt(datosHora[2]);
		Date fec = null;
		try {
			fec = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse(fecha + " " + hora);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return fec;
	}
}

